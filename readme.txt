ngn/k is a simple, small, fast vector programming language for x86_64 GNU/linux
license: GNU AGPLv3 (v3 only) - https://bitbucket.org/ngn/k/src/master/LICENSE
usage: rlwrap ./k repl.k

k.h  header
g.h  header generated by g.k
m.c  memory manager and main()
p.c  parser
b.c  bytecode compiler and virtual machine
k.c  eval/apply/mend: . @
a.c  arithmetic verbs
o.c  order and equivalence: <x >x x~y
f.c  find/random: x?y
h.c  shape-related: x#y ,x x,y
s.c  object to string
i.c  i/o: 0: 1: <fd >fd
v.c  the rest of the verbs
w.c  adverbs
c.c  system commands: \w \t:n ..
j.c  json parser: `j?x
e.c  error handling
t/   tests
g/   https://codegolf.stackexchange.com/
e/   https://projecteuler.net/
a15/ https://adventofcode.com/2015
a19/ https://adventofcode.com/2019
o/   .o files (build tmp)

CONTRIBUTING
For bug reports and ideas for improvement please use
 https://bitbucket.org/ngn/k/issues
If you also want to contribute code to this project, first you must read
carefully, fill in, and sign one of the two copyright assignment
agreements:
 * HA-CAA-I if doing it as an individual
 * HA-CAA-E if it's on behalf of a company or other legal entity
then send it via email to the address printed by the following k expression:
 ".@acgiklmnov"@9 5 3 6 0 9 5 3 6 10 7 10 11 1 4 8 2 5 7 0 3 10 8
and wait for confirmation.
NOTE about section 3 (d) from the agreements:
ngn/k accepts only contributions for which the contributor owns the copyright
of the entire work of authorship and is at liberty to transfer it.
If you aren't sure about the legal status of your work, do let us know.
If you are employed or studying, we advise you obtain an explicit written
permission to contribute to this project from your employer or educational
institution.

INTRO
k objs
 A - the type of a tagged ptr to a k obj
 x y z u - var names for A objs. usually x y z are the args and u is the result from the fn.
 xt xn xb - type, length, and bucket index of x (similarly: yt,zn..)
 xtc xtL xtX.. - is x of type tc,tL,tX..?
 xtt - is x an atom? (not dict)
 xtT - is x a list? (not table)
 xR xr - refcount++ and --. if it drops to 0, free the obj (recursively if necessary)
 xc xl xd xa.. - content of x as a char*,long*,double*,A*..
 xci xlj.. - i-th or j-th typed element: xc[i],xl[j]..
calling conventions
 A0 A1 A2 A3 - the types of functions with 0..3 args, accepting and returning type A.
  all args are consumed (their refcounts are decremented or they are returned as result).
  macros: A0(f,body) A1(f,body).. define such fns with args names x,y,z
 AA - like above but accepts a ptr to A and length; arg names: a and n
 AX - like AA but also accepts a separate arg called x, which is *not* consumed
 fns with ugly names (eg trailing "_" or uppercase) might not follow the conventions
 /*0*/ after an arg means "not consumed", /*1*/ means "consumed"
errors
 error-reporting fns return null after consuming their args:
  err("msg",x,y,z) - generic error. x,y,z are optional
  et(x,y,z) el() en().. - type error, length error, nyi error.. x,y,z are optional
  etn(a,n) eln(a,n).. - variants that consume n objs from the memory pointed by a
 error-pass-through macros:
  N(expr) - if expr evaluates to null, N() returns from the current function,
            otherwise N(expr) is the same as expr
  Nx(expr) - same as N(), but if there's an error it consumes x

COMPARISON
with other k impls: https://ngn.bitbucket.io/k.html
